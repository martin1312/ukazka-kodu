<?php if(!defined('MIGRATIONS')) die('Unauthorized access'); 

class MigratorManager 
{
    private $database = null;
    private $logger = null;

    public static $config = [
        'migrations_table'  => 'custom_migrations',
        'app_main_db'       => 'main_db',
    ];
    
    public static $runtime_config = [
        'execute'     => true,
    ];

    public function __construct(Database $database, Logger $logger) 
    {
        $this->database = $database;
        $this->logger = $logger;
    }
    
    /**
     * runs migrations
     */
    public function run() : void
    {
                
        $this->logger->log('Migration run started [Execution: '.(self::$runtime_config['execute'] ? 'ALLOWED' : 'DISABLED').']');
        
        $files = $this->get_migration_files();
                
        if(count($files) <= 0) {
            throw new Exception('No migration file to execute');
        }
        
        //load names from migrations table
        $executed = $this->get_executed_migrations();
        $executed_names = [];
        foreach($executed as $item) {
            $executed_names[] = $item['name'];
        }
                        
        foreach ($files as $file) {
            //check is executed
            if(in_array($file, $executed_names)) {
                $this->logger->log(' file migration "'.$file.'" - skipped');
                continue;
            }
            
            //mark as executed
            if(self::$runtime_config['execute']) {
                $this->mark_migration_executed($file);
            }
            
            $this->logger->log(' file migration "'.$file.'" - executing');
            
            if(self::$runtime_config['execute']) {
                include(MIGRATIONS_PATH . DIRECTORY_SEPARATOR . $file .'.php');                
            }
        }
        
        $this->logger->log('Migration run end');
    }
    
    /**
     * Show info about executed migrations
     */
    public function get_migration_status() : array
    {
        $files = $this->get_migration_files();
        
        $executed = $thi->get_executed_migrations();
        $executed_names = [];
        $executed_simple = [];
        foreach($executed as $item) {
            $executed_names[$item['name']] = $item;
            $executed_simple[] = $item['name'];
        }
                
        $data['files_info'] = [];
        
        foreach ($files as $file) {    
            $info = [
                'name' => $file,
                'executed' => false,
                'created' => null,
            ];
            
            if(array_key_exists($file, $executed_names)) {
                $info['executed'] = true;
                $info['created'] = $executed_names[$file]['created'];
            }
            
            $data['files_info'][] = $info;
        }
                
        $data['diff_names'] = array_diff($executed_simple, $files);

        return $data;        
    }
    
    /**
     * Finds php files with migration content
     */
    public function get_migration_files() : array
    {
        $files = [];
        foreach (glob(MIGRATIONS_PATH . DIRECTORY_SEPARATOR . "v*.php") as $file) {
            $files[] = basename($file, '.php');
        }
        sort($files);
        
        return $files;
    }

    /**
     * Creates internal migration table in DB necessary to store migration information
     */
    public function create_migrations_table() : void
    {

        $this->database->query('USE '.self::$config['app_main_db']);
        
        $sql = include(TOOL_PATH.DIRECTORY_SEPARATOR.'migrations_table.php');   
        $this->database->query($sql);  
    }
    
    /**
     * Mark one migration as executed
     */
    private function mark_migration_executed($name) : void
    {
        $this->database->query('USE '.self::$config['app_main_db']);
        $sql = "INSERT INTO ".self::$config['migrations_table']." (name, created) VALUES (:name, :date)";
        $data = [
            'name'  => $name,
            'date'  => date('Y-m-d H:i:s'),
        ];
        $stmt = $this->database->prepare($sql)->execute($data); 
    }    
    
    /**
     * Return array of executed migrations
     */
    public function get_executed_migrations() : array
    {
        $this->database->query('USE '.self::$config['app_main_db']);

        $sql = "SELECT * FROM `".self::$config['migrations_table']."`";
        $stmt = $this->database->prepare($sql)->execute();

        $result_data = $stmt->fetchAll();
        
        return $result_data;
    }

    /**
     * Return all databases
     */
    public function get_all_databases() : array
    {
        $dbs = [];        

        $stmt = $this->database->query("SHOW DATABASES");
        $result = $stmt->fetchAll();

        foreach($result as $item) {
            $dbs[] = $item['Database'];
        }
        
        return $dbs;  
    }
}

