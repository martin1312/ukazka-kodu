<?php if(!defined('MIGRATIONS')) die('Unauthorized access'); 

interface Migrations 
{
    
    public function getConfig() : array;
    
    public function sql() : string;
    
    public function post_action($affected_databases);
    
}